#include <Arduino.h>
#include <limits.h>
#include <DHT.h>
#include <NewPing.h>

#define MAX_READINGS 10
#define MAX_INTERVAL 10000

#define SOUND_PIN A0
#define LIGHT_PIN A1
#define ECHO_PIN 3
#define TRIG_PIN 4
#define MOTION_PIN 2
#define DHT_PIN 5

long long int avg_sound;
int min_sound, max_sound;

long long int avg_distance;
long int min_distance, max_distance;

long long int avg_temp;
long int avg_humidity;

int ir_motion_count;

long long int avg_light;
int min_light, max_light;

int interval_ms = 0;
int num_readings = 0;

long int lastMillis = 0;

//---

long int cur_sound, cur_light, cur_distance;
DHT dht(DHT_PIN, DHT11);
NewPing sonar(TRIG_PIN, ECHO_PIN, 2000);

//---

void resetInterval()
{
	num_readings = 0;

	avg_sound = 0;
	avg_distance = 0;
	avg_temp = 0;
	avg_humidity = 0;
	avg_light = 0;

	ir_motion_count = 0;

	min_light = INT_MAX;
	max_light = INT_MIN;
	min_distance = INT_MAX;
	max_distance = INT_MIN;
	min_sound = INT_MAX;
	max_sound = INT_MIN;
}

//labels = ['location_id', 'avg_distance', 'min_distance', 'max_distance', 'avg_temperature', 'avg_humidity', 'avg_light', 'min_light', 'max_light', 'ir_motion_count', 'avg_sound', 'min_sound', 'max_sound', 'interval_seconds', 'time_collected']
void sendData()
{
  long int currentMillis = millis();

	Serial.print(avg_distance / float(num_readings));
	Serial.print(' ');
	Serial.print(min_distance);
	Serial.print(' ');
	Serial.print(max_distance);
	Serial.print(' ');
	Serial.print(avg_temp / float(num_readings));
	Serial.print(' ');
	Serial.print(avg_humidity / float(num_readings));
	Serial.print(' ');
	Serial.print(avg_light / float(num_readings));
	Serial.print(' ');
	Serial.print(min_light);
	Serial.print(' ');
	Serial.print(max_light);
	Serial.print(' ');
	Serial.print(ir_motion_count);
	Serial.print(' ');
	Serial.print(avg_sound / float(num_readings));
	Serial.print(' ');
	Serial.print(min_sound);
	Serial.print(' ');
	Serial.print(max_sound);
	Serial.print(' ');
	Serial.print(interval_ms);
	Serial.print(' ');
	Serial.print(currentMillis - lastMillis);
  Serial.println();
  lastMillis = millis();
}

void setup()
{
  pinMode(SOUND_PIN, OUTPUT);
  pinMode(ECHO_PIN, INPUT);
  pinMode(TRIG_PIN, OUTPUT);
  pinMode(MOTION_PIN, INPUT);
  pinMode(DHT_PIN, INPUT);

	Serial.begin(115200);
	resetInterval();
}

void loop()
{
	//check for the end of an interval
	if (num_readings == MAX_READINGS || interval_ms >= MAX_INTERVAL)
	{
		sendData();
		resetInterval();
	}

	/*handle sound*/
	cur_sound = analogRead(SOUND_PIN);
	min_sound = min(min_sound, cur_sound);
	max_sound = max(max_sound, cur_sound);
	avg_sound += cur_sound;

	/*handle light*/
	cur_light = analogRead(LIGHT_PIN);
	min_light = min(min_light, cur_light);
	max_light = max(max_light, cur_light);
	avg_light += cur_light;

	/*handle distance*/
	//triger sound wave
	digitalWrite(TRIG_PIN, HIGH);
	delay(10);
	digitalWrite(TRIG_PIN, LOW);

	//read in echo
	cur_distance = sonar.ping_median(3);  
  min_distance = min(min_distance, cur_distance);
  max_distance = max(max_distance, cur_distance);
  avg_distance += cur_distance;

	/*handle temp & humidity*/
	avg_temp += dht.readTemperature();
	avg_humidity = dht.readHumidity();

	/*handle PIRsensor*/
	if (digitalRead(MOTION_PIN) == HIGH)
		ir_motion_count++;

	//increment number of readings obtained so far
	num_readings++;

	delay(5);
}
