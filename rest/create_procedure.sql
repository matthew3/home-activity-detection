DELIMITER $$

DROP PROCEDURE IF EXISTS database_project.rollup_time $$

CREATE PROCEDURE 
  database_project.rollup_time(
	IN delta_seconds INTEGER,
	IN location INTEGER
	)
BEGIN
	SET @stmt = CONCAT('SELECT location_id,
		FROM_UNIXTIME(FLOOR(UNIX_TIMESTAMP(time_collected) / ', delta_seconds, ') * ', delta_seconds, ') AS grp,

		AVG(avg_distance) AS avg_dist,
		MIN(min_distance) AS min_dist,
		MAX(max_distance) AS max_dist,

		AVG(avg_temperature) AS avg_temp,
		MIN(avg_temperature) AS min_temp,
		MAX(avg_temperature) AS max_temp,

		AVG(avg_humidity) AS avg_humidity,
		MIN(avg_humidity) AS min_humidity,
		MAX(avg_humidity) AS max_humidity,

		AVG(avg_light) AS avg_light,
		MIN(min_light) AS min_light,
		MAX(max_light) AS max_light,

		AVG(ir_motion_count) AS avg_motion_count,
		MIN(ir_motion_count) AS min_motion_count,
		MAX(ir_motion_count) AS max_motion_count,

		AVG(avg_sound) AS avg_sound,
		MIN(min_sound) AS min_sound,
		MAX(max_sound) AS max_sound,

		AVG(interval_ms) AS avg_iterval,
		SUM(num_readings) AS num_readings

	FROM database_project.data GROUP BY location_id, grp');

	IF (location IS NOT NULL) THEN
		SET @stmt = CONCAT(@stmt, ' HAVING location_id=', location);
	END IF;

	PREPARE stmtPrep FROM @stmt;
	EXECUTE stmtPrep;
	DEALLOCATE PREPARE stmtPrep;
END $$

DELIMITER ;
