CREATE OR REPLACE VIEW database_project.clean_data AS
SELECT
	avg_humidity / 5.1 AS humidity_percent,
	avg_light AS avg_light_index,
	max_light AS max_light_index,
	min_light AS min_light_index
FROM database_project.data
