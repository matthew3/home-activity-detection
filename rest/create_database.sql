DROP DATABASE IF EXISTS database_project;
CREATE DATABASE database_project;
USE database_project;

GRANT ALL ON `database_project`.* TO 'project_admin'@'%' IDENTIFIED BY 'project';

CREATE TABLE locations (
	id INTEGER NOT NULL AUTO_INCREMENT,
	street_number INTEGER,
	street_name VARCHAR(20),
	street_type ENUM('Ave','Blvd','Rd','St','Fwy'),
	postal VARCHAR(7),
	phone VARCHAR(12),
	name VARCHAR(30),
	room VARCHAR(30),
		PRIMARY KEY (id)
);

CREATE TABLE data (
	id INTEGER NOT NULL AUTO_INCREMENT,
	location_id INTEGER NOT NULL,
		FOREIGN KEY location_fk (location_id) REFERENCES locations (id),
	avg_distance FLOAT(7,3) DEFAULT NULL,
	min_distance FLOAT(7,3) DEFAULT NULL,
	max_distance FLOAT(7,3) DEFAULT NULL,
	avg_temperature FLOAT(5,3) DEFAULT NULL,
	avg_humidity FLOAT(5,3) DEFAULT NULL,
	min_light SMALLINT DEFAULT NULL,
	max_light SMALLINT DEFAULT NULL,
	avg_light FLOAT(7,3) DEFAULT NULL,
	ir_motion_count SMALLINT DEFAULT NULL,
	avg_sound FLOAT(7,3) DEFAULT NULL,
	min_sound SMALLINT DEFAULT NULL,
	max_sound SMALLINT DEFAULT NULL,
	interval_ms INTEGER NOT NULL,
	num_readings INTEGER NOT NULL,
	time_collected DATETIME DEFAULT NULL,
	srv_time_collected DATETIME NOT NULL,
		PRIMARY KEY (id)
);

CREATE TRIGGER `time_collected_default` BEFORE INSERT ON  `data` 
FOR EACH ROW 
SET NEW.srv_time_collected = NOW()
