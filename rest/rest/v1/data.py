from flask import request, jsonify;

from data_db import db_connect;

ERR_INTERNAL_SERVER = 500;
ERR_PAGE_NOT_FOUND = 404;
RESPONSE_OK = 200;
ERR_IN_REQUEST = 400;

labels = ['id', 'location_id', 'avg_distance', 'min_distance', 'max_distance', 'avg_temperature', 'avg_humidity','avg_light', 'min_light', 'max_light', 'ir_motion_count', 'avg_sound', 'min_sound', 'max_sound', 'num_readings', 'interval_ms', 'time_collected','srv_time_collected']
#@app.route("/restful/v0.1/user/", methods=["POST"])
def data_entry_POST():	
	args = request.get_json(force=True);

	values = [ str(args.get(labels[i], 'DEFAULT')) for i in xrange(len(labels)) ]
	values[-2] = "'" + values[-2] + "'"

	#insert the new mastery
	conn = db_connect();
	with conn:

		x = conn.cursor();
		sql = r"INSERT INTO data VALUES (%s)" % ','.join(values)

		#try to insert, could fail on title being same as another
		try:
			x.execute(sql);

			conn.commit();
		#failed on duplicate title, get mastery that contains title
		except Exception as e:
			conn.rollback();
			return jsonify(**{'msg': "An error has occurred: " + str(e) + str(values) + sql});

		return jsonify(**{'success': 'Added'});

	return error(ERR_INTERNAL_SERVER);
