import serial
import requests
import json
from time import strftime, gmtime
import sys

comm = serial.Serial("/dev/ttyACM0", 115200)
labels = ['location_id', 'avg_distance', 'min_distance', 'max_distance', 'avg_temperature', 'avg_humidity','avg_light', 'min_light', 'max_light', 'ir_motion_count', 'avg_sound', 'min_sound', 'max_sound', 'interval_ms', 'num_readings', 'time_collected']

url = 'http://208.79.218.101/rest/v1/data/'
cache = []

comm.flushInput(); #get rid of everything currently in the Serial comm
comm.readline(); #throw out the first line so that we're starting fresh after a newline
try:
	while True:
		line = comm.readline().strip()
		if not line:
			continue;

		try:
			values = map(float, line.split())

			values.insert(0, sys.argv[1])
			values.append(strftime("%Y-%m-%d %H:%M:%S", gmtime()))

			print values;

			cache.append(values)
			print len(cache)
		except:
			pass;

		while cache:
			entry = cache[0]
			payload = dict(zip(labels, entry))
			try:
				r = requests.post(url, data=json.dumps(payload))
				if r.status_code == 200:
					r.pop(0)
				else:
					break;

			except requests.exceptions.RequestException as e:
				print e
				break;
except:
	comm.close();
