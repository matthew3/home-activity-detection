from flask import Flask
from flask.ext.cors import CORS

app = Flask(__name__);
CORS(app);

from rest.v1 import data as d;

app.add_url_rule("/rest/v1/data/", view_func=d.data_entry_POST, methods=["POST"]);

if __name__ == "__main__":
	app.debug=True;
	#app.run(host="127.0.0.1", port=8080);
	app.run(host="208.79.218.101", port=8080);
